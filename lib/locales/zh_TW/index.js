var zh_TW = {};
module['exports'] = zh_TW;
zh_TW.title = "Chinese (Taiwan)";
zh_TW.separator = "";
zh_TW.address = require("./address");
zh_TW.name = require("./name");
zh_TW.phone_number = require("./phone_number");
zh_TW.lorem = require("./lorem");
